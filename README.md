# EjercicioPython

Ejercicio BBVA

Ejercicio de python practico.
La practica consiste en retornar la siguiente información:

"id","name","last_name","startDate","work"
"0001","Jose Alberto","Escalona","8-04-2021","Chedraui"
"0002","Nadia","Buendia","03-04-2021","Walmart"
"0003","Pablo Saul","Ramirez","01-04-2020","GoNet"
"0004","Santiago","Perea","02-12-2021","Devlyn"
"0005","Brenda","Samperio","03-04-2021","Walmart"

Instrucciones:

1.-Retornar todos los valores.
2.-Crear una función que logre retornar por las cabeceras del archivo.
3.-Crea un método que de como salida un json con los valores que acabas de crear.


Nota. El archivo de testing no se elaboro por tiempo de entrega
